package com.kantar.employee.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kantar.employee.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
  Optional<Employee>  findByEmail(String email);
  List<Employee> findByDepartment(String department);
  /*
  @Query("SELECT e from Employee e where e.salary <= ?1")
  List<Employee> findByMinSalary(double salary); */
  
  @Query(value = "SELECT * FROM emp_details where emp_salary <= ?1", nativeQuery = true)
  List<Employee> findByMinSalary(double salary);
  /*
   * save
   * update
   * findById(int id)
   * findAll
   */
}
