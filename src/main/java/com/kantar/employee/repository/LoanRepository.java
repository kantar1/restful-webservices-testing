package com.kantar.employee.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kantar.employee.entity.Loan;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Integer> {
	
	Optional<Loan> findByName(String name);

}
