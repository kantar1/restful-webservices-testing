package com.kantar.employee.exception;

public class EmployeeNotFoundException extends Exception {
	
	public EmployeeNotFoundException(String msg) {
		super(msg);
	}

}
