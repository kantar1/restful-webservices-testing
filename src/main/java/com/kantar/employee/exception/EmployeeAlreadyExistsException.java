package com.kantar.employee.exception;

public class EmployeeAlreadyExistsException extends Exception {
	
	public EmployeeAlreadyExistsException(String msg) {
	super(msg);
	}
	
}
