package com.kantar.employee.exception;

public class LoanAlreadyExistsException extends Exception {

	public LoanAlreadyExistsException(String msg) {
		super(msg);
	}
}
