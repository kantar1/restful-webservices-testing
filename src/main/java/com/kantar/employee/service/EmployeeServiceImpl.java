package com.kantar.employee.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kantar.employee.entity.Employee;
import com.kantar.employee.exception.EmployeeAlreadyExistsException;
import com.kantar.employee.exception.EmployeeNotFoundException;
import com.kantar.employee.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository empRepository;

	@Override
	public Employee save(Employee employee) throws EmployeeAlreadyExistsException {
		Optional<Employee> findByEmail = this.empRepository.findByEmail(employee.getEmail());
		if (findByEmail.isPresent())
			throw new EmployeeAlreadyExistsException("Employee Already Exists With this Email :" + employee.getEmail());
		else
			return this.empRepository.save(employee);
	}

	@Override
	public List<Employee> getAll() {
		return this.empRepository.findAll();
	}

	@Override
	public Employee getEmployeeById(int id) throws EmployeeNotFoundException{
		Optional<Employee> findById = this.empRepository.findById(id);
		if (!findById.isPresent())
			throw new EmployeeNotFoundException("Employee Not Found With this Id: " + id );		
		return this.empRepository.findById(id).get();
	}

	@Override
	public Employee update(Employee employee) throws EmployeeNotFoundException{
		Optional<Employee> findById = this.empRepository.findById(employee.getId());
		if (!findById.isPresent())
			throw new EmployeeNotFoundException("Employee Not Found With this Id: " + employee.getId() );		
		
		return this.empRepository.save(employee);
	}

	@Override
	public boolean delete(int id) throws EmployeeNotFoundException {
		Optional<Employee> findById = this.empRepository.findById(id);
		if (!findById.isPresent())
			throw new EmployeeNotFoundException("Employee Not Found With this Id: " + id );		
		
		this.empRepository.delete(findById.get());
		return true;
	}

}
