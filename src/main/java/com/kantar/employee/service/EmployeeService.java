package com.kantar.employee.service;

import java.util.List;

import com.kantar.employee.entity.Employee;
import com.kantar.employee.exception.EmployeeAlreadyExistsException;
import com.kantar.employee.exception.EmployeeNotFoundException;

public interface EmployeeService {
	
	public Employee save(Employee employee) throws EmployeeAlreadyExistsException;
	public List<Employee> getAll();
	public Employee getEmployeeById(int id) throws EmployeeNotFoundException;
	public Employee update(Employee employee) throws EmployeeNotFoundException;
	public boolean delete(int id) throws EmployeeNotFoundException;

}
