package com.kantar.employee.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "emp_details")
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "emp_id")
	private int id;
	
	@Column(name="emp_name")
	@NotBlank(message = "name is required")
	@Size(min = 2, max = 50, message = "Name must be between 2 and 50 characters")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Name must contain only alphabets")
	private String name;
	
	
	@Column(name = "emp_email")
	@NotBlank(message = "Email is required")
    @Email(message = "Invalid email format")
	private String email;
	
	@Column(name = "emp_contactno")
	@NotNull(message = "Phone number is required")
    @Pattern(regexp = "^[0-9]+$", message = "Phone number must contain only numeric characters")
   	private String contactNo;
	
	@Column(name = "emp_dept")
	@NotBlank(message = "department is required")
	@Size(min = 2, max = 50, message = "department must be between 2 and 50 characters")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "department must contain only alphabets")
   	private String department;
	
	
	@Column(name = "emp_salary")
	@NotNull(message = "salary is required")
    @Positive(message = "salary must be a positive value")
	
	private double salary;
	/*
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "loan_id", referencedColumnName = "loan_id")
    private Loan loan;
	*/
	/*
	@OneToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "emp_loan_details", joinColumns = {@JoinColumn(name="emp_id")},inverseJoinColumns = {@JoinColumn(name="loan_id")})
	private List<Loan> loansList;
	*/
	/*
	@ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "emp_loan_details",
               joinColumns = @JoinColumn(name = "emp_id"),
               inverseJoinColumns = @JoinColumn(name = "loan_id"))
    private List<Loan> loans; */

}
